// https://gist.github.com/jeromecoupe/0b807b0c1050647eb340360902c3203a

'use strict';

const
  dir = {
    src: 'src/',
    build: './'
  },

  // Gulp and plugins
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  newer         = require('gulp-newer'),
  imagemin      = require('gulp-imagemin'),
  sass          = require('gulp-sass'),
  postcss       = require('gulp-postcss'),
  deporder      = require('gulp-deporder'),
  concat        = require('gulp-concat'),
  stripdebug    = require('gulp-strip-debug'),
  uglify        = require('gulp-uglify'),
  phplint       = require('gulp-phplint'),
  browsersync = require("browser-sync").create()
;


// BrowserSync
function browserSync(done) {
  browsersync.init({
    proxy       : 'localhost:8888/elysio-toolkit',
    files       : dir.build + '**/*',
    open        : true,
    notify      : true,
    ghostMode   : false,
    ui: {
      port: 8001
    }
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}




// PHP settings
const phpConf = {
  src           : dir.src + 'template/**/*.php',
  build         : dir.build
};


// PHP task
function php() {
  return gulp
    .src(phpConf.src)
    .pipe(newer(phpConf.build))
    .pipe(phplint())
    .pipe(gulp.dest(phpConf.build))
    .pipe(browsersync.stream());
}





// image settings
// const imagesConf = {
//   src         : dir.src + 'images/**/*',
//   build       : dir.build + 'images/'
// };

// // image processing
// function images() {
//   return gulp
//     .src(imagesConf.src)
//     .pipe(newer(imagesConf.build))
//     .pipe(imagemin())
//     .pipe(gulp.dest(imagesConf.build));
// }




// CSS settings
var cssConf = {
  src         : dir.src + 'sass/style.scss',
  watch       : dir.src + 'sass/**/*',
  build       : dir.build + 'assets/css/',
  sassOpts: {
    outputStyle     : 'nested',
    precision       : 3,
    errLogToConsole : true
  },
  processors: [
    require('css-mqpacker'),
    require('cssnano')
  ]
};

// CSS processing
function css(){
  return gulp
    .src(cssConf.src)
    .pipe(sass(cssConf.sassOpts))
    .pipe(postcss(cssConf.processors))
    .pipe(gulp.dest(cssConf.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
}




// JS





// Watch files
function watchFiles() {
  gulp.watch(phpConf.src, php);
  gulp.watch(cssConf.watch, css);
}


const build = gulp.parallel(php, css);
const watch = gulp.parallel(watchFiles, browserSync);

// export tasks
exports.php = php;
exports.css = css;
//exports.image = image;
exports.build = build;
exports.watch = watch;
exports.default = build;
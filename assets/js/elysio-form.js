( function( $ ) {
	
	// Make sure you run this code under Elementor.
	$( window ).on( 'elementor/frontend/init', function() {
		
		$('body').on('focus', '.ewrap input, .ewrap textarea', function(){
		  $(this).parents('.ewrap').addClass('is-filled');
		  $(this).parents('.ewrap').addClass('is-focused');
		});
		$('body').on('blur', '.ewrap input, .ewrap textarea', function(){
		  $(this).parents('.ewrap').removeClass('is-focused');
		  if( $(this).val().length == 0 ){
		  $(this).parents('.ewrap').removeClass('is-filled');
		  }
		});

		if ($('.ectf7-custom-style2').length > 0) {
			jQuery('.ectf7-custom-style2 .ewrap').each(function(){
			    //console.log( jQuery(this).find('.elabel').width())
			    //console.log( parseInt(jQuery(this).find('.wpcf7-form-control').css('padding-right')) )
			    
			    var newPaddingLeft = jQuery(this).find('.elabel').width() + 2 * parseInt(jQuery(this).find('.wpcf7-form-control').css('padding-right'));

			    jQuery(this).find('.wpcf7-form-control').css('padding-right', newPaddingLeft + 'px')

			});
		}


		/* Material Design Button Ribble */
		$("html").on("click", ".wpcf7 form.ectf7-material-button input[type='submit']", function(evt) {
		  var btn = $(evt.currentTarget);
		  var x = evt.pageX - btn.offset().left;
		  var y = evt.pageY - btn.offset().top;
		  
		  var duration = 1000;
		  var animationFrame, animationStart;
		  
		  var animationStep = function(timestamp) {
		    if (!animationStart) {
		      animationStart = timestamp;
		    }
		   
		    var frame = timestamp - animationStart;
		    if (frame < duration) {
		      var easing = (frame/duration) * (2 - (frame/duration));
		      
		      var circle = "circle at " + x + "px " + y + "px";
		      var color = "rgba(0, 0, 0, " + (0.3 * (1 - easing)) + ")";
		      var stop = 90 * easing + "%";

		      btn.css({
		        "background-image": "radial-gradient(" + circle + ", " + color + " " + stop + ", transparent " + stop + ")"
		      });

		      animationFrame = window.requestAnimationFrame(animationStep);
		    } else {
		      $(btn).css({
		        "background-image": "none"
		      });
		      window.cancelAnimationFrame(animationFrame);
		    }
		    
		  };
		  
		  animationFrame = window.requestAnimationFrame(animationStep);

		});


	} );


} )( jQuery );
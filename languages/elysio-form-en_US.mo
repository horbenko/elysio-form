��    u      �  �   l      �	  5   �	  /   
     G
  l   T
     �
  
   �
     �
     �
     �
  
                  *     <     J     _     l     �     �     �     �     �     �     �  )   �          '     7     F     U     ]  	   d     n  8   z     �     �     �     �               4     J     c     o     u     �     �  �   �     4  
   9     D     T     `     m     {     �     �     �     �                 7     T     p  
   �     �     �     �     �     �     I     O     `     l     }     �     �     �     �     �     �     �     �     �                          "     &     .  %   F     l     r     {     �     �     �     �     �     �     �  
   �  
   �     �     �     �     �          '     B  
   H     S     W     p     �  �  �  5   f  /   �     �  l   �     F  
   U     `     m     �  
   �     �     �     �     �     �     �     �               ,     ;     M     _     l  )   s     �     �     �     �     �     �  	   �     �  8   �     8     F     _     t     �     �     �     �     �     �     �            �   (     �  
   �     �     �     �     �                ;     V     p     �      �     �     �     �  
                  $     5     N     �     �     �     �                         0     B     T     \     _     u     �     �     �     �     �     �     �  %   �     �     �                               #     +     3  
   ;  
   F     Q     ]     t     �     �     �     �  
   �     �     �     �          ?   P       ,   1         D                         l   Y   7   t       E       0   _   2       ]   .           f      6   &   <          n                           A      Z   U   d   r   #       o   B         +               N   `   q   g   R          "          V   =   h   C       	   W               !   X                       s   4          O      L                 $   c   ^   e   S   5   M      [       j   %   k   a   3   m      *   G   /   K   I   p       (           '   @       F   :         T   -   H       
      u   Q   )   i       >   9   ;   b   8                    J      \    "%1$s" requires "%2$s" to be installed and activated. "%1$s" requires "%2$s" version %3$s or greater. Active Color Add ContactForm7 widget for Elementor. Also you can modify layout, design & styles. And even add animations. Artem Horbenko Background Border Color Border Color Focused Border Radius Box Shadow Button Button Align Button Background Button Border Button Border Radius Button Color Button Hover Border Color Button Hover Color Button Margin Button Padding Button Text Align Button Typography Button Width Center Color for label when input field in focus Contact Form 7 Contactf Form 7 Custom Style 1 Custom Style 2 Default Design Elementor Elysio Form Elysio Form Widget & Styles Contact Form 7 for Elementor Error Message Error Message Background Error Message Border Error Message Border Radius Error Message Color Error Message Margin Error Message Padding Error Message Typography Field Width Focus Focus Color Focus Color 2 Focused Label Color For use preloaded styles please update Contact Form 7 markup, see <a href="http://forms.elysio.top/?page_id=45" target="_blank">the example</a>. Form Form Align Form Background Form Margin Form Padding Form Response Form Response Border Radius Form Response Error Background Form Response Error Border Form Response Error Color Form Response Margin Form Response Padding Form Response Success Background Form Response Success Border Form Response Success Color Form Response Typography Form Width Hide Hover Input & Textarea Inputfield Invalid Color Insert the URL where you want users to redirect to when the contact form is submitted and is successful. Leave Blank to Disable Label Label Box Shadow Label Color Label Typography Left Margin Material Design Material Design 1 Material Design 2 Material Design 3 New Tab No No Contact Form found No Pages Found! None Normal Notifications Offsets PHP Padding Paragraph Bottom Offset Redirect URL, After Successfully Sent Right Settings Show Style Style 1 Style 2 Style 3 Style 4 Style 5 Style 6 Text Align Text Color Text Shadow Textarea Border Radius Textarea Height Textarea Max Height Textarea Min Height Textarea Responsive Height Title Typography Yes https://form.elysio.top/ https://google.com https://horbenko.com/ Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Elysio Form Widget & Styles Contact Form 7 for Elementor
PO-Revision-Date: 2021-01-10 13:55+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: elysio-form.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: en
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 "%1$s" requires "%2$s" to be installed and activated. "%1$s" requires "%2$s" version %3$s or greater. Active Color Add ContactForm7 widget for Elementor. Also you can modify layout, design & styles. And even add animations. Artem Horbenko Background Border Color Border Color Focused Border Radius Box Shadow Button Button Align Button Background Button Border Button Border Radius Button Color Button Hover Border Color Button Hover Color Button Margin Button Padding Button Text Align Button Typography Button Width Center Color for label when input field in focus Contact Form 7 Contactf Form 7 Custom Style 1 Custom Style 2 Default Design Elementor Elysio Form Elysio Form Widget & Styles Contact Form 7 for Elementor Error Message Error Message Background Error Message Border Error Message Border Radius Error Message Color Error Message Margin Error Message Padding Error Message Typography Field Width Focus Focus Color Focus Color 2 Focused Label Color For use preloaded styles please update Contact Form 7 markup, see <a href="http://forms.elysio.top/?page_id=45" target="_blank">the example</a>. Form Form Align Form Background Form Margin Form Padding Form Response Form Response Border Radius Form Response Error Background Form Response Error Border Form Response Error Color Form Response Margin Form Response Padding Form Response Success Background Form Response Success Border Form Response Success Color Form Response Typography Form Width Hide Hover Input & Textarea Inputfield Invalid Color Insert the URL where you want users to redirect to when the contact form is submitted and is successful. Leave Blank to Disable Label Label Box Shadow Label Color Label Typography Left Margin Material Design Material Design 1 Material Design 2 Material Design 3 New Tab No No Contact Form found No Pages Found! None Normal Notifications Offsets PHP Padding Paragraph Bottom Offset Redirect URL, After Successfully Sent Right Settings Show Style Style 1 Style 2 Style 3 Style 4 Style 5 Style 6 Text Align Text Color Text Shadow Textarea Border Radius Textarea Height Textarea Max Height Textarea Min Height Textarea Responsive Height Title Typography Yes https://form.elysio.top/ https://google.com https://horbenko.com/ 
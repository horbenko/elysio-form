<?php
namespace ElysioForm\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elysio Form
 *
 * Elementor widget for elysio form.
 *
 * @since 1.0.0
 */
class Elysio_Contact_Form7 extends Widget_Base {

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'contact-form7';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Contact Form 7', 'elysio-form' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-form-horizontal';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	// public function get_script_depends() {
	// 	return [ 'elysio-form' ];
	// }

	// TODO why not work?
    public function get_help_url() {
        return 'https://form.elysio.top/' . $this->get_name();
    }

	/* Get list of avalible Contact Form 7 */
	private function get_contact_form_list() {
		$args = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
		$rs = array();
		if( $data = get_posts($args)){
			foreach($data as $key){
				$id = $key->ID;
				$name = $key->post_title;
				$rs[$id] = $name;
			}
		}else{
			$rs[0] = __('No Contact Form found', 'elysio-form');
		}
		return $rs;
	}

	private function elysio_get_all_pages(){

		$args = array('post_type' => 'page', 'posts_per_page' => -1);

		  $catlist=[];
		  
		  if( $categories = get_posts($args)){
		    foreach ( $categories as $category ) {
		      (int)$catlist[$category->ID] = $category->post_title;
		    }
		  }
		  else{
		      (int)$catlist['0'] = esc_html__('No Pages Found!', 'void');
		  }
		return $catlist;
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends() {
		return [ 'elysio-form' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section( 
			'section_content', [ 'label' => __( 'Contactf Form 7', 'elysio-form' ), ] 
		);
		$this->add_control(
			'ctf7',
			[
			'label'		=> __( 'Contactf Form 7', 'elysio-form' ),
			'description' => 'Choose from list or create <a href="'. site_url('/wp-admin/admin.php?page=wpcf7-new') . '" target="_blank">New Contact Form</a>. Contact Form 7 - plugin must be installed.',
			'type'    => Controls_Manager::SELECT,
			'label_block' => true,
			'multiple'    => true,
			'options' => $this->get_contact_form_list(),
			]
		);
		$this->add_control(
			'form_design',
			[
				'label' => __( 'Design', 'elysio-form' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default'  => __( 'Default', 'elysio-form' ),
					'custom-style1'  => __( 'Custom Style 1', 'elysio-form' ),
					'custom-style2'  => __( 'Custom Style 2', 'elysio-form' ),
					'material1'  => __( 'Material Design 1', 'elysio-form' ),
					'material2'  => __( 'Material Design 2', 'elysio-form' ),
					'material3'  => __( 'Material Design 3', 'elysio-form' ),
				],
			]
		);
		$this->add_control(
			'form_design_note',
			[
				'type' => Controls_Manager::RAW_HTML,
				'raw' => __( 'For use preloaded styles please update Contact Form 7 markup, see <a href="https://form.elysio.top/?page_id=45" target="_blank">the example</a>.', 'elysio-form' ),
				'content_classes' => 'elementor-control-field-description',
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'default'
						]
					]
				],
			]
		);

		$this->add_control(
			'form_redirect',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'form_redirect_external',
			[
				'label' => __( 'Redirect URL, After Successfully Sent', 'elysio-form' ),
				'description' => esc_html__('Insert the URL where you want users to redirect to when the contact form is submitted and is successful. Leave Blank to Disable','elysio-form'),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'https://google.com', 'elysio-form' ),
				'label_block' => 1,
			]
		);
		$this->end_controls_section();




		/* Form Styles */

		$this->start_controls_section(
			'section_style_form',
			[
				'label' => __( 'Form', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'form_background',
				'label' => __( 'Form Background', 'elysio-form' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} form',
			]
		);
		$this->add_control(
			'form_align',
			[
				'label' => __( 'Form Align', 'elysio-form' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-form' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-form' ),
						'icon' => 'eicon-h-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-form' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'selectors' => [
				  '{{WRAPPER}} form' => 'margin: {{VALUE}}',
				],
				'selectors_dictionary' => [
				  'left' => '0 auto 0 0',
				  'center' => '0 auto',
				  'right' => '0 0 0 auto',
				],
			]
		);
		$this->add_responsive_control(
			'contactform_width',
			[
				'label'	=> __( 'Form Width', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' ],
				'default'	=> [
					'unit'	=> '%',
					'size'	=> 100,
				],
				'selectors'	=> [
					'{{WRAPPER}} form' => 'width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_responsive_control(
			'form_border_radius',
			[
				'label' => __( 'Border Radius', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} form' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'form_margin',
			[
				'label' => __( 'Form Margin', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} form' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'form_padding',
			[
				'label' => __( 'Form Padding', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} form' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'form_shadow',
				'label' => __( 'Box Shadow', 'elysio-form' ),
				'selector' => '{{WRAPPER}} form',
			]
		);


		$this->add_control(
			'form_hr',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'all_typo',
				'label' => __( 'Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}}, {{WRAPPER}} p, {{WRAPPER}} input, {{WRAPPER}} textarea',
				'separator' => 'before',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'default'
						],
					]
				],
			]
		);
		$this->add_control(
			'all_color',
			[
				'label' => __( 'Text Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}, .wpcf7-form-control:not([type=submit])' => 'color: {{VALUE}} !important',
				],
			]
		);
		$this->add_responsive_control(
			'form_text_align',
			[
				'label' => __( 'Text Align', 'elysio-form' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-form' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-form' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-form' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'selectors' => [
				  '{{WRAPPER}}, {{WRAPPER}} input, {{WRAPPER}} textarea' => 'text-align: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'paragraph_offset',
			[
				'label' => __( 'Paragraph Bottom Offset', 'elysio-form' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'unit' => 'em',
					'size' => 0.5,
				],
				'selectors' => [
					'{{WRAPPER}} p' => 'margin-bottom: {{SIZE}}{{UNIT}} !important;',
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'default'
						]
					]
				],
			]
		);
		$this->end_controls_section();




		/* Input */

		$this->start_controls_section(
			'section_style_inputfield',
			[
				'label' => __( 'Input & Textarea', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'inputfield_typography',
				'label' => __( 'Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .wpcf7-form-control:not([type=submit])',
			]
		);

		$this->add_control(
			'inputfield_color',
			[
				'label' => __( 'Text Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				],
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'textfield_background',
				'label' => __( 'Background', 'elysio-form' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .wpcf7-form-control:not([type=submit]), {{WRAPPER}} .wpcf7-form-control:not([type=submit]):focus, {{WRAPPER}} .wpcf7 form.ectf7-material3 .ewrap.is-filled .elabel',
			]
		);

		$this->add_control(
			'inputfield_border_color',
			[
				'label' => __( 'Border Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'default' => '#C9C9C9',
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'border-color: {{VALUE}}',

					'{{WRAPPER}} .ectf7-material1 .wpcf7-form-control:not([type=submit])' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}});',

					'{{WRAPPER}} .ectf7-material2 .wpcf7-form-control:not([type=submit])' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}});',
				],
			]
		);

		$this->add_control(
			'inputfield_active_color',
			[
				'label' => __( 'Active Color', 'elysio-form' ),
				'description' => __( 'Color for label when input field in focus', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'default' => '#42a5f5',
				'selectors' => [
					'{{WRAPPER}} .wpcf7 form.ectf7-material1 .ewrap.is-focused .elabel' => 'color: {{VALUE}}',
					'{{WRAPPER}} .wpcf7 form.ectf7-material2 .ewrap.is-focused .elabel' => 'color: {{VALUE}}',
					'{{WRAPPER}} .wpcf7 form.ectf7-material3 .ewrap.is-focused .elabel' => 'color: {{VALUE}}',

					'{{WRAPPER}} .ectf7-material1 .wpcf7-form-control:not([type=submit]):focus' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{inputfield_border_color.VALUE}} 0,{{inputfield_border_color.VALUE}});',

					'{{WRAPPER}} .ectf7-material2 .wpcf7-form-control:not([type=submit])' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{inputfield_border_color.VALUE}} 0,{{inputfield_border_color.VALUE}});',

					'{{WRAPPER}} .wpcf7 form.ectf7-material3 .wpcf7-form-control:not([type=submit]):focus' => 'border-color: {{VALUE}}',
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material3'
						]
					]
				],
			]
		);

		$this->add_control(
			'inputfield_border_focused_color',
			[
				'label' => __( 'Border Color Focused', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#42a5f5',
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit]):focus' => 'border-color: {{VALUE}}',

					'{{WRAPPER}} .wpcf7 form.ectf7-custom-style1 .ewrap .wpcf7-form-control:not([type=submit]):focus' => 'border-color: {{VALUE}}',

					'{{WRAPPER}} .wpcf7 form.ectf7-custom-style2 .ewrap .wpcf7-form-control:not([type=submit]):focus' => 'border-color: {{VALUE}}',
				],
				'default'	=> '#000000',
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material3'
						]
					]
				],
			]
		);

		$this->add_responsive_control(
			'inputfield_border_radius',
			[
				'label' => __( 'Border Radius', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .ewrap .wpcf7-form-control:not([type=submit])' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
					]
				],
			]
		);

		$this->add_responsive_control(
			'textarea_border_radius',
			[
				'label' => __( 'Textarea Border Radius', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} textarea.wpcf7-form-control:not([type=submit])' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .ewrap textarea.wpcf7-form-control:not([type=submit])' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
					]
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'	=> 'inputfield_border',
				'label'	=> __( 'Button Border', 'elysio-form' ),
				'selector'	=> '{{WRAPPER}} .wpcf7 .wpcf7-form-control:not([type=submit])',
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
					]
				],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'inputfield_box_shadow',
				'label' => __( 'Box Shadow', 'elysio-form' ),
				'selector' => '{{WRAPPER}} .wpcf7 .wpcf7-form-control:not([type=submit])',
			]
		);

		$this->add_control(
			'inputfield_sizes_hr',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_responsive_control(
			'inputfield_width',
			[
				'label'	=> __( 'Field Width', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' ],
				'default'	=> [
					'unit'	=> '%',
					'size'	=> 100,
				],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'width: 100% !important; max-width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$this->add_responsive_control(
			'inputfield_margin',
			[
				'label' => __( 'Margin', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'inputfield_padding',
			[
				'label' => __( 'Padding', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control:not([type=submit])' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .wpcf7-not-valid-tip' => 'padding-left: {{LEFT}}{{UNIT}};',


					'{{WRAPPER}} .wpcf7 form.ectf7-material1 .ewrap .elabel' => 'left: {{LEFT}}{{UNIT}}; transform: translateY({{TOP}}{{UNIT}}) scale(1);',
					'{{WRAPPER}} .wpcf7 form.ectf7-material1 .ewrap.is-filled .elabel' => 'transform: translateY(0) scale(0.75);',

					'{{WRAPPER}} .wpcf7 form.ectf7-material2 .ewrap .elabel' => 'left: {{LEFT}}{{UNIT}}; transform: translateY({{TOP}}{{UNIT}}) scale(1);',
					'{{WRAPPER}} .wpcf7 form.ectf7-material2 .ewrap.is-filled .elabel' => 'transform: translateY(9px) scale(0.75);',
					
					'{{WRAPPER}} .wpcf7 form.ectf7-material3 .ewrap .elabel' => 'left: {{LEFT}}{{UNIT}}; transform: translateY({{TOP}}{{UNIT}}) scale(1);',
					'{{WRAPPER}} .wpcf7 form.ectf7-material3 .ewrap.is-filled .elabel' => 'transform: translateY(-4px) scale(0.75);',
				],
			]
		);


		$this->add_control(
			'inputfield_textarea_hr',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_responsive_control(
			'textarea_height',
			[
				'label'	=> __( 'Textarea Height', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' , 'em'],
				'selectors'	=> [
					'{{WRAPPER}} textarea' => 'height: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
			]
		);
		$this->add_responsive_control(
			'textarea_responsive_height',
			[
				'label' => __( 'Textarea Responsive Height', 'elysio-form' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'your-plugin' ),
				'label_off' => __( 'No', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_responsive_control(
			'textarea_min_height',
			[
				'label'	=> __( 'Textarea Min Height', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' , 'em'],
				'selectors'	=> [
					'{{WRAPPER}} textarea' => 'min-height: {{SIZE}}{{UNIT}};',
				],
				'default'	=> [
					'unit'	=> 'em',
					'size'	=> 2.6,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [ 'textarea_responsive_height!' => false ],
			]
		);
		$this->add_responsive_control(
			'textarea_max_height',
			[
				'label'	=> __( 'Textarea Max Height', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' , 'em'],
				'selectors'	=> [
					'{{WRAPPER}} textarea' => 'max-height: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [ 'textarea_responsive_height!' => 'no' ],
			]
		);
		$this->end_controls_section();




		/* Label Styles */

		$this->start_controls_section(
			'section_style_label',
			[
				'label' => __( 'Label', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'label_color_black',
			[
				'label' => __( 'Label Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} label' => 'color: {{VALUE}}',
				],
				'default'	=> '#000000',
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material3'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style2'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style3'
						],
					]
				],
			]
		);

		$this->add_control(
			'label_color_gray',
			[
				'label' => __( 'Label Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} label' => 'color: {{VALUE}}',
				],
				'default'	=> '#777777',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material3'
						],
					]
				],
			]
		);

		$this->add_control(
			'label_color_white',
			[
				'label' => __( 'Label Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elabel' => 'color: {{VALUE}}',
				],
				'default'	=> '#ffffff',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style2'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style3'
						],
					]
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'label_typography',
				'label' => __( 'Label Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} label',
			]
		);
		$this->add_responsive_control(
			'label_margin',
			[
				'label' => __( 'Offsets', 'elysio-form' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'material3'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style2'
						],
						[
							'name' => 'form_design',
							'operator' => '!=',
							'value' => 'custom-style3'
						],
					]
				],
			]
		);


		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'label_background',
				'label' => __( 'Background', 'elysio-form' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} form .ewrap .elabel',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style2'
						],
					]
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'label_box_shadow',
				'label' => __( 'Label Box Shadow', 'elysio-form' ),
				'selector' => '{{WRAPPER}} .elabel',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style2'
						],
					]
				],
			]
		);




		$this->add_control(
			'label_focused_color',
			[
				'label' => __( 'Focused Label Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .is-focused .elabel' => 'color: {{VALUE}} !important',
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style2'
						],
					]
				],
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'label_focused_background',
				'label' => __( 'Background', 'elysio-form' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} form .ewrap.is-focused .elabel',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'custom-style2'
						],
					]
				],
			]
		);

		$this->end_controls_section();




		/* Button */

		$this->start_controls_section(
			'section_style_button',
			[
				'label' => __( 'Button', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
	
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'btn_typo',
				'label' => __( 'Button Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .wpcf7-submit',
			]
		);
		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button_shadow',
				'label' => __( 'Text Shadow', 'elysio-form' ),
				'selector' => '{{WRAPPER}} .wpcf7-submit',
			]
		);

		$this->add_control(
			'btn_material_design',
			[
				'label' => __( 'Material Design', 'elysio-form' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'your-plugin' ),
				'label_off' => __( 'No', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material1'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material2'
						],
						[
							'name' => 'form_design',
							'operator' => '==',
							'value' => 'material3'
						],
					]
				],
			]
		);


		$this->start_controls_tabs(
			'style_tabs'
		);
			$this->start_controls_tab(
				'style_normal_tab',
				[
					'label' => __( 'Normal', 'elysio-form' ),
				]
			);
				$this->add_control(
					'btn_color',
					[
						'label' => __( 'Button Color', 'elysio-form' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}} input[type=submit]' => 'color: {{VALUE}} !important',
						],
					]
				);
				$this->add_group_control(
					Group_Control_Background::get_type(),
					[
						'name' => 'btn_background',
						'label' => __( 'Button Background', 'elysio-form' ),
						'types' => [ 'classic', 'gradient' ],
						'selector' => '{{WRAPPER}} input[type=submit]',
					]
				);
			$this->end_controls_tab();


			$this->start_controls_tab(
				'style_hover_tab',
				[
					'label' => __( 'Hover', 'elysio-form' ),
				]
			);
				$this->add_control(
					'btn_hover_color',
					[
						'label' => __( 'Button Hover Color', 'elysio-form' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#39414d',
						'selectors' => [
							'{{WRAPPER}} input[type=submit]:hover, {{WRAPPER}} input[type=submit]:focus, {{WRAPPER}} input[type=submit]:active' => 'color: {{VALUE}} !important',
						],
					]
				);

				$this->add_group_control(
					Group_Control_Background::get_type(),
					[
						'name' => 'btn_hover_background',
						'label' => __( 'Button Background', 'elysio-form' ),
						'types' => [ 'classic', 'gradient' ],
						'selector' => '{{WRAPPER}} input[type=submit]:hover, {{WRAPPER}} input[type=submit]:focus, {{WRAPPER}} input[type=submit]:active',
					]
				);
				$this->add_control(
					'btn_hover_border_color',
					[
						'label' => __( 'Button Hover Border Color', 'elysio-form' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#39414d',
						'selectors' => [
							'{{WRAPPER}} input[type=submit]:hover, {{WRAPPER}} input[type=submit]:focus, {{WRAPPER}} input[type=submit]:active' => 'border-color: {{VALUE}} !important;',
						],
					]
				);
			$this->end_controls_tab();
		$this->end_controls_tabs();





		$this->add_responsive_control(
			'btn_border_radius',
			[
				'label'	=> __( 'Button Border Radius', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} input[type=submit].wpcf7-submit'	=> 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'	=> 'btn_border',
				'label'	=> __( 'Button Border', 'elysio-form' ),
				'selector'	=> '{{WRAPPER}} .wpcf7 input[type=submit].wpcf7-submit',
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'label' => __( 'Box Shadow', 'elysio-form' ),
				'selector' => '{{WRAPPER}} .wpcf7 input[type=submit].wpcf7-submit',
			]
		);

		$this->add_control(
			'hr-buttons2',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'btn_align',
			[
				'label' => __( 'Button Align', 'elysio-form' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-form' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-form' ),
						'icon' => 'eicon-h-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-form' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'default' => 'left',
				'selectors' => [
				  '{{WRAPPER}} input[type=submit].wpcf7-submit' => 'margin: {{VALUE}}',
				],
				'selectors_dictionary' => [
				  'left' => '0 auto 0 0',
				  'center' => '0 auto',
				  'right' => '0 0 0 auto',
				],
			]
		);
		$this->add_responsive_control(
			'btn_text_align',
			[
				'label' => __( 'Button Text Align', 'elysio-form' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-form' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-form' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-form' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'selectors' => [
				  '{{WRAPPER}} input[type=submit].wpcf7-submit' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'btn_width',
			[
				'label'	=> __( 'Button Width', 'elysio-form' ),
				'type'	=> Controls_Manager::SLIDER,
				'size_units'	=> [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'	=> [
					'{{WRAPPER}} input[type=submit].wpcf7-submit' => 'width: {{SIZE}}{{UNIT}} !important;',
				],
			]
		);
		$this->add_responsive_control(
			'btn_margin',
			[
				'label'	=> __( 'Button Margin', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} input[type=submit].wpcf7-submit' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_responsive_control(
			'btn_padding',
			[
				'label'	=> __( 'Button Padding', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} input[type=submit].wpcf7-submit' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);
			
		$this->end_controls_section();




		/* Notifications Styles */

		$this->start_controls_section(
			'section_style_notifications',
			[
				'label' => __( 'Notifications', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'inputfield_invalid_color',
			[
				'label' => __( 'Inputfield Invalid Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-not-valid.wpcf7-form-control:not([type=submit])' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} form.ectf7-material1 .wpcf7-not-valid.wpcf7-form-control:not([type=submit])' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}});',
					'{{WRAPPER}} form.ectf7-material2 .wpcf7-not-valid.wpcf7-form-control:not([type=submit])' => 'background-image: linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}}),linear-gradient(180deg,{{VALUE}} 0,{{VALUE}});',
				],
				'default'	=> '#E53E3E',
			]
		);

		$this->add_control(
			'error_message_toggle',
			[
				'label' => __( 'Error Message', 'elysio-form' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'error_message_typography',
				'label' => __( 'Error Message Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .wpcf7-not-valid-tip',
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_control(
			'error_message_color',
			[
				'label' => __( 'Error Message Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-not-valid-tip' => 'color: {{VALUE}} !important;',
				],
				'default'	=> '#E53E3E',
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_control(
			'error_message_bg',
			[
				'label' => __( 'Error Message Background', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-not-valid-tip' => 'background-color: {{VALUE}} !important;',
				],
				'default'	=> '#ffffff',
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'	=> 'error_message_border',
				'label'	=> __( 'Error Message Border', 'elysio-form' ),
				'selector'	=> '{{WRAPPER}} .wpcf7-not-valid-tip',
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_responsive_control(
			'error_message_border_radius',
			[
				'label'	=> __( 'Error Message Border Radius', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-not-valid-tip'	=> 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_responsive_control(
			'error_message_margin',
			[
				'label'	=> __( 'Error Message Margin', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-not-valid-tip' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'condition' => [ 'error_message_toggle' => 'yes' ],
			]
		);
		$this->add_responsive_control(
			'error_message_padding',
			[
				'label'	=> __( 'Error Message Padding', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-not-valid-tip' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		/* Rename to validation ? */
		$this->add_control(
			'hr_form_notification',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'form_response_toggle',
			[
				'label' => __( 'Form Response', 'elysio-form' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_response_typography',
				'label' => __( 'Form Response Typography', 'elysio-form' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .wpcf7-response-output',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);


		$this->add_control(
			'form_response_invalid_color',
			[
				'label' => __( 'Form Response Error Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form.invalid .wpcf7-response-output' => 'color: {{VALUE}} !important',
				],
				'default'	=> '#E53E3E',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_control(
			'form_response_invalid_bg',
			[
				'label' => __( 'Form Response Error Background', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form.invalid .wpcf7-response-output' => 'background-color: {{VALUE}} !important',
				],
				'default'	=> '#FCECEC',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'	=> 'form_response_invalid_border',
				'label'	=> __( 'Form Response Error Border', 'elysio-form' ),
				'selector'	=> '{{WRAPPER}} .wpcf7-form.invalid .wpcf7-response-output',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);


		$this->add_control(
			'form_response_valid_color',
			[
				'label' => __( 'Form Response Success Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-response-output' => 'color: {{VALUE}} !important',
				],
				'default'	=> '#fff',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_control(
			'form_response_valid_bg',
			[
				'label' => __( 'Form Response Success Background', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-response-output' => 'background-color: {{VALUE}} !important',
				],
				'default'	=> '#61CE70',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'	=> 'form_response_valid_border',
				'label'	=> __( 'Form Response Success Border', 'elysio-form' ),
				'selector'	=> '{{WRAPPER}} .wpcf7-response-output',
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);

		
		$this->add_control(
			'form_response_colors',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);


		$this->add_responsive_control(
			'form_response_border_radius',
			[
				'label'	=> __( 'Form Response Border Radius', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-response-output' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_responsive_control(
			'form_response_margin',
			[
				'label'	=> __( 'Form Response Margin', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-response-output' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);
		$this->add_responsive_control(
			'form_response_padding',
			[
				'label'	=> __( 'Form Response Padding', 'elysio-form' ),
				'type'	=> Controls_Manager::DIMENSIONS,
				'size_units'	=> [ 'px', '%', 'em' ],
				'selectors'	=> [
					'{{WRAPPER}} .wpcf7-response-output' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
				'condition' => [ 'form_response_toggle' => 'yes' ],
			]
		);

		$this->end_controls_section();




		/* Focus Styles */

		$this->start_controls_section(
			'section_style_focus',
			[
				'label' => __( 'Focus', 'elysio-form' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'focus_style',
			[
				'label' => __( 'Style', 'elysio-form' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'none'  => __( 'None', 'elysio-form' ),
					'default'  => __( 'Default', 'elysio-form' ),
					'focus-solid'  => __( 'Style 1', 'elysio-form' ),
					'focus-dotted'  => __( 'Style 2', 'elysio-form' ),
					'rounded-focus'  => __( 'Style 3', 'elysio-form' ),
					'colored-focus-alpha'  => __( 'Style 4', 'elysio-form' ),
					'colored-focus-beta'  => __( 'Style 5', 'elysio-form' ),
					'colored-focus-gamma'  => __( 'Style 6', 'elysio-form' ),
				],
			]
		);
		$this->add_control(
			'focus_color',
			[
				'label' => __( 'Focus Color', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ectf7-focus-focus-solid input:focus, {{WRAPPER}} .ectf7-focus-focus-solid textarea:focus' => 'outline-color: {{VALUE}} !important',
					'{{WRAPPER}} .ectf7-focus-focus-dotted input:focus, {{WRAPPER}} .ectf7-focus-focus-dotted textarea:focus' => 'outline-color: {{VALUE}} !important',
					'{{WRAPPER}} .ectf7-focus-rounded-focus input:focus, {{WRAPPER}} .ectf7-focus-rounded-focus textarea:focus' => 'box-shadow: 0 0 0 3px {{VALUE}} !important;',
					'{{WRAPPER}} .ectf7-focus-colored-focus-alpha input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-alpha textarea:focus' => 'box-shadow: 2px 2px 7px {{VALUE}}, -2px -2px 7px rgba(241, 103, 4, 0.75) !important;',
					'{{WRAPPER}} .ectf7-focus-colored-focus-beta input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-beta textarea:focus' => 'box-shadow: 3px 3px 15px 2px {{VALUE}}, -3px -3px 15px 1px rgba(241, 103, 4, 0.75) !important;',
					'{{WRAPPER}} .ectf7-focus-colored-focus-gamma input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-gamma textarea:focus' => 'box-shadow: 0 0 0 3px rgba(255, 255, 255, 0.87), 3px 3px 18px 0px {{VALUE}}, -3px -3px 20px 0px rgba(241, 103, 4, 0.75) !important;',
				],
				'default'	=> 'rgba(233, 32, 134, 0.65)',
			]
		);
		$this->add_control(
			'focus_color_a',
			[
				'label' => __( 'Focus Color 2', 'elysio-form' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ectf7-focus-colored-focus-alpha input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-alpha textarea:focus' => 'box-shadow: 2px 2px 7px {{focus_color.VALUE}}, -2px -2px 7px {{VALUE}} !important;',
					'{{WRAPPER}} .ectf7-focus-colored-focus-beta input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-beta textarea:focus' => 'box-shadow: 3px 3px 15px 2px {{focus_color.VALUE}}, -3px -3px 15px 1px {{VALUE}} !important;',
					'{{WRAPPER}} .ectf7-focus-colored-focus-gamma input:focus, {{WRAPPER}} .ectf7-focus-colored-focus-gamma textarea:focus' => 'box-shadow: 0 0 0 3px {{focus_color.VALUE}}, 3px 3px 18px 0px {{focus_color.VALUE}}, -3px -3px 20px 0px {{VALUE}} !important;',
				],
				'default'	=> 'rgba(0, 0, 0, 0.2)',
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'focus_style',
							'operator' => '==',
							'value' => 'colored-focus-alpha'
						],
						[
							'name' => 'focus_style',
							'operator' => '==',
							'value' => 'colored-focus-beta'
						],
						[
							'name' => 'focus_style',
							'operator' => '==',
							'value' => 'colored-focus-gamma'
						]
					]
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$html_class = 'ewpcf7';

		$form_uniqid = uniqid('ewpcf7-');

		/* Add form design class */
		$html_class .= ' ectf7-' . $settings['form_design'] . ' '. $form_uniqid;

		if( $settings['btn_material_design'] == 'yes' ){
			$html_class .= ' ectf7-material-button';
		}
		if( $settings['textarea_responsive_height'] != 'yes' ){
			$html_class .= ' ectf7-textarea-resize-none';
		}
		if( $settings['focus_style'] != 'default' ){
			$html_class .= ' ectf7-focus-' . $settings['focus_style'];
		}

		
		if( $settings['error_message_toggle'] != 'yes' ){
			$html_class .= ' ectf7-hide-error-message';
		}
		if( $settings['form_response_toggle'] != 'yes' ){
			$html_class .= ' ectf7-hide-form-response';
		}


		/* If has form render it */
		if( $settings['ctf7'] ){
			echo( do_shortcode('[contact-form-7 
				id="'. $settings['ctf7'] .'"
				html_class="' . $html_class. '"
			]') );
		}
		?>
	
			<script>
				document.addEventListener('wpcf7mailsent', function(event) {

					jQuery('.<?php echo $form_uniqid; ?> .is-filled').removeClass('is-filled');

					<?php
					if( !empty($settings['form_redirect_external']) ) :  ?>
						if( '<?php echo $settings['ctf7']; ?>' == event.detail.contactFormId){
							location = '<?php 
						    if( !empty($settings['form_redirect_external']) ){
								echo $settings['form_redirect_external'];
						    }
						    ?>';
						}
					<?php
					endif;
					?>

				}, false);
			</script>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
	}
}

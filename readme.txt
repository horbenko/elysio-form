=== Elysio Form - Widget & Styles Contact Form 7 for Elementor ===
Contributors: horbenko
Tags: Contact Form, Elementor, Contact Form 7, Contact Form 7 Widget, Elementor Addons, Widget, Contact, Addons, contact-form-7
Requires at least: 4.0
Tested up to: 5.6
Stable tag: 5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Adds Elysio Form that are specifically designed to be used in conjunction with the Elementor Page Builder and Contact Form 7. [Live preview](https://form.elysio.top/?page_id=49)

== Features ==
* Preloaded Design Styles, like Google Material Design
* Floating Labels & Animations
* Form Elements Styling and Custom Styling Options
* Drag and drop the widget. Show Contact form in any place on your website
* Easy Contact form 7 pickup
* Text, Typography and Background Styling
* Button Styling, material ripple effect
* Style Your Messages and Form Notifications
* After Success Sent Redirect to URL
* Style Focus state

== Frequently Asked Questions ==

= Install: =
1. Go to the WordPress Dashboard "Add New Plugin" section.
2. Search For "Elysio Forms - Widget & Styles Contact Form 7 for Elementor".
3. Install, then Activate it.

= OR: =
1. Unzip (if it is zipped) and Upload `elysio-forms` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

= How to use = 
1. Drag and drop widget Elysio Contact Form 7 to any place on your website using Elementor Live Page Builder.
2. Choose form dropdown list or create a new one first.
3. Choose defaut style that provided your theme OR Chose design style that you like. Don't forget to use [Elysio Form Markup](https://form.elysio.top/?page_id=45) for preloaded designs.
4. Custom any style option for Form, input fields, button, labels, notifications and focus state.

== Need Help? ==
Is there any feature that you want to get in this plugin? 
Needs assistance to use this plugin? 
Feel free to [Contact us](https://form.elysio.top/contact-us/)


== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is stored in the /assets directory.
2. This is the second screen shot